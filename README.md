### countfiles-plugin

##### maven编写plugn统计项目的某一文件类型的数量

```java
@Parameter(property = "currentBaseDir",defaultValue = "User/pathHome")
private String currentBaseDir;  // 这个参数是引入本插件的工程传进来的文件夹

@Parameter(property = "suffix",defaultValue = ".java")
private String suffix;  // 这个参数是引入本插件的工程传入进来的文件类型 (例如.java)
```

##### 使用

```xml
<plugin>
	<groupId>com.lb.mygupao</groupId>
	<artifactId>countfiles-plugin</artifactId>
	<version>1.0-SNAPSHOT</version>
    <configuration>
        <!--${basedir}为系统环境变量：当前项目根目录-->
		<currentBaseDir>${basedir}</currentBaseDir>
		<suffix>.java</suffix>
    </configuration>
    <executions>
        <execution>	
            <goals>
                <goal>count-files</goal>
            </goals>
            <!-- clean phase执行 -->
            <phase>clean</phase>
        </execution>
    </executions>
</plugin>

```

